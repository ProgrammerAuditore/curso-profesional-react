export default function Pelicula(props) {

    return (
    <div className="movie-item-style-2">
        <img src={props.portada} alt="" />
        <div className="mv-item-infor">
            <h6><a href="moviesingle.html">{props.titulo}<span>(2012)</span></a></h6>
            <p className="rate"><i className="ion-android-star"></i><span>{props.calificacion}</span> /10</p>
            <p className="describe">{props.children}</p>
            <p className="run-time"> 
            Run Time: {props.duracion} ’    .     
            <span>MMPA: {props.clasificacion} </span>    .     
            <span>Release: {props.lanzamiento} </span></p>
            <p>Director: {props.director} </p>
            <p>Stars: {props.actores} </p>
        </div>
    </div>
    );

}
