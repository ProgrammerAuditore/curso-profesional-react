import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './App.css';
import ListadoPeliculas from './views/ListadoPeliculas';
import Blog from './views/Blog';

function App() {
  return (

    <Router>
    <Switch>
      <Route  exact path="/">
        <ListadoPeliculas />
      </Route>
      <Route exact path="/blog">
        <Blog />
      </Route>
    </Switch>
    </Router>
    
   
  );
}

export default App;
