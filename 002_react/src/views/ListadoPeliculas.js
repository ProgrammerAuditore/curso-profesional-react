import { useEffect , useState } from 'react';
import '../App.css';
import Pelicula from '../components/Pelicula';
import PageWrapper from '../PageWrapper';
import Peliculas from '../peliculas.json';
import Paginacion from '../components/Paginacion';

function ListadoPeliculas() {
  
  const [paginaActual, setPaginaActual] = useState(1);
  const peliculasPorPagina = 2;
  let peliculas = Peliculas;

  /** Está funciona se va ejecutar solamente cuando se cargue la página  **/
  useEffect(()=>{
    cargarPeliculas();
  },[]);


  const cargarPeliculas = () => {
    peliculas = peliculas.slice(
      (paginaActual-1) * peliculasPorPagina, 
      paginaActual * peliculasPorPagina
    );
  }

  const getTotalPaginas = () => {
    return Math.ceil(Peliculas.length / peliculasPorPagina);
  }

  return (
    <PageWrapper>

      {
        peliculas.map( (pelicula) => {
          return <Pelicula titulo={pelicula.titulo} calificacion={pelicula.calificacion} portada={pelicula.portada}
            director={pelicula.director} actores={pelicula.actores}
            clasificacion={pelicula.clasificacion} duracion={pelicula.duracion} lanzamiento={pelicula.lanzamiento}>
              { pelicula.descripcion }
          </Pelicula>;
        })
      }

    <Paginacion pagina={paginaActual} total={getTotalPaginas()} onChange={(pagina)=>{  
      setPaginaActual(pagina)
     }} />
     
    </PageWrapper>
  );
}

export default ListadoPeliculas;
