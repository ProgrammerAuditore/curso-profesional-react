import { useState } from 'react';
import html2canvas from 'html2canvas';
import logo from './logo.svg';
import './App.css';

function App() {

  // Crear una variable y una funcion para modificar
  // la variable usando State de React. 
  const [linea1, setLinea1 ] = useState('');
  const [linea2, setLinea2 ] = useState('');
  const [img, setImg ] = useState('');
  

  // Crear una función
  const fncOnChangeLinea1 = function (e) {
    setLinea1(e.target.value);
  }

  // Crear una función
  const fncOnChangeLinea2 = function (e) {
    setLinea2(e.target.value);
  }

  // Crear una función
  const fncOnChangeImg = function (e) {
    setImg(e.target.value);
  }

  const fncExportarImg = function(e){
    html2canvas(document.querySelector("#meme")).then(canvas => {
      let img = canvas.toDataURL("image/png");
      let link = document.createElement("a");
      link.download = 'meme.png';
      link.href = img;
      link.click();
    });
  }

  return (
    <div className="App">
      <select onChange={fncOnChangeImg}>
        <option value="hc">Casa en llamas</option>
        <option value="boy">Futurama</option>
        <option value="cat">Casa en llamas</option>
        <option value="smart">Casa en llamas</option>
      </select><br/>

      <input onChange={fncOnChangeLinea1} type="text" placeholder="Linea 1"/><br/>
      <input onChange={fncOnChangeLinea2}  type="text" placeholder="Linea 2"/><br/>
      <button onClick={fncExportarImg} >Exportar</button> <br/>

      <div id="meme">
        <div className="contenedor-meme">
          <img src={"/memes/"+img+".jpg"} className="img-meme" />
          <span className="linea1">{linea1}</span>
          <span className="linea2">{linea2}</span>
        </div>
      </div>
    </div>
  );
}

export default App;
